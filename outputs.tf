output "target_pool_link" {
  value = "${google_compute_target_pool.backend_service.self_link}"
}
output "https_health_check_link" {
  value = "${google_compute_https_health_check.health_check.self_link}"
}
