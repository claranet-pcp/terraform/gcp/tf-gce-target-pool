variable "service" {
  description = "The name of the service, suffixes the name attribute on resources created by this module"
  type = "string"
}

variable "managed_instance_group1" {
  description = "The name of the first managed instance group to be part of this pool"
  type = "string"
}

variable "managed_instance_group2" {
  description = "The name of the second managed instance group to be part of this pool"
  type = "string"
}

variable "managed_instance_group3" {
  description = "The name of the third managed instance group to be part of this pool"
  type = "string"
}
