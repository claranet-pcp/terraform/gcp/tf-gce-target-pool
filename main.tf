resource "google_compute_target_pool" "target_pool" {
  name        = "backend-${var.service}"

  instances = [  
    "${var.managed_instance_group1}",
    "${var.managed_instance_group2}",
    "${var.managed_instance_group3}" 
  ]

  health_checks = ["${google_compute_https_health_check.health_check.self_link}"]
}

resource "google_compute_https_health_check" "health_check" {
  name               = "healthcheck-${var.service}"
  request_path       = "/healthcheck"
  check_interval_sec = 5
  timeout_sec        = 5
}
